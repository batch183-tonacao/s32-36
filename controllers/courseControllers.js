const Course = require("../models/Course");
const auth = require("../auth");

// Create new course
/*
	Steps:
	1. create a new course object using the mongoose model and the informationfrom the request body
	2. save the new User to the database
*/

module.exports.addCourse = (reqBody) =>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})
	return newCourse.save().then((course, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Retrieve all courses
/*
	Step"
	1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result => result);
}

module.exports.getAllActive = () =>{
	return Course.find({isActive: true}).then(result => result);
}

module.exports.getCourse = (courseId)=>{
	return Course.findById(courseId).then(result => result);
}

module.exports.updateCourse = (courseId, reqBody) =>{
	// Specify fields to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	}
	// syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdate, error) => {
	if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.archiveCourse = (courseId) =>{
	// Specify fields to be updated
	let updatedStatus = {
		isActive: false
	}
	return Course.findByIdAndUpdate(courseId, updatedStatus).then((courseUpdate, error) => {
	if(error){
			return false;
		}
		else{
			return true;
		}
	})
}