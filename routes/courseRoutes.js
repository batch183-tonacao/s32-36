const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js");

// Route for creating a course
router.post("/", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		return res.send("You are not authorized to create a course.");
	}
})
router.get("/all", auth.verify, (req, res)=>{
	courseControllers.getAllCourses().then(resultFromController => res.send(resultFromController));
})

router.get("/", (req, res)=>{
	courseControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

router.get("/:courseId", (req, res) =>{
	courseControllers.getCourse(req.params.courseId).then(resultFromController => res.send(resultFromController));
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) =>{
	courseControllers.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})

// Route to archive a course
router.patch("/:courseId/archive", auth.verify, (req, res) =>{
	courseControllers.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;

